# visio-care

Une plateforme de visioconférence / réunion en ligne optimisé facilitation et inclusion.
- Gestion/visibilisation du temps de parole
- Gestion/visibilisation de l'état émotionnel
- Optimisation de montée en charge > 1000 participant.e.s
- Logiciel libre, au service de sa communauté pour toujours.


## Contribuer à concrétiser ce projet :
- [Décrivez l'outil de visioconférence de vos rêves](https://framagit.org/1000i100/visio-care/-/issues) pour que ce projet soit au service du plus grand nombre.
- [Décrivez ce que vous aimez dans ce que vous avez déjà eu l'occasion d'utiliser](https://framagit.org/1000i100/visio-care/-/issues) pour s'inspirer de l'existant là ou il fait correctement le travail.
- [Décrivez ce qui vous frustre, vous agace dans les outils que vous utilisez aujourd'hui](https://framagit.org/1000i100/visio-care/-/issues) pour pour ne pas reproduire les même erreurs.
- [Indiquez vos compétences, ressources et intension d'implication](https://framagit.org/1000i100/visio-care/-/wikis/human-ressources)
- [Contribuer financièrement, c'est précieux aussi. Donnez dès maintenant !](mailto:contact@1forma-tic.fr)